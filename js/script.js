(function(){
  var scripts = {
    app: '/js/spotifyApp/app.js',
    ajax: '/js/spotifyApp/ajax.js',
    view: '/js/spotifyApp/view.js'
  }

  function createScript(url){
    var fScript = document.querySelector('script');
    var p = new Promise(function(resolve, reject){
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.onload = function(){
          resolve();
        }
        fScript.parentElement.insertBefore(script, fScript);   
    });
    return p;
  }

  Promise.all([
    createScript(scripts.app),
    createScript(scripts.ajax),
    createScript(scripts.view)
  ]).then(function(){
    VIEW.init();
    });
})();
