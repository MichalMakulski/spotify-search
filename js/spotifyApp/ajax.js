var AJAX = (function() {
  
  var xmlhttp;
  
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  

  function getData(src, query, callback) {
    xmlhttp.onreadystatechange = callback;
    xmlhttp.open("GET", src + query, true);
    xmlhttp.send();
  }
  
  return {
    xmlhttp: xmlhttp,
    getData: getData
  }
  
})();
