var APP = (function(){
  
  // base urls for ajax call:
  var mainUrl = 'https://api.spotify.com/v1/search?q=';
  var artistUrl = 'https://api.spotify.com/v1/artists/';
  var albumUrl = 'https://api.spotify.com/v1/albums/'

  function ajaxHandler(callback) {
    if(AJAX.xmlhttp.readyState === XMLHttpRequest.DONE) {
      if(AJAX.xmlhttp.status === 200) {
        callback(JSON.parse(AJAX.xmlhttp.responseText));
      }else {
        alert('There was a problem with the request.');
      }
    }
  }
  
  return {
    urls: {
      main: mainUrl,
      artist: artistUrl,
      album: albumUrl
    },
    ajaxHandler: ajaxHandler
  }
  
})();