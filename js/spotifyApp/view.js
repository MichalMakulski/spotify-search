var VIEW = (function(){
  
  // html elements:
  var searchInput = document.querySelector('#search');
  var suggestionsList = document.querySelector('#suggestions');
  var searchBtn = document.querySelector('.search-btn');
  var results = document.querySelector('.results');
  var albums = document.querySelector('.albums');
  var htmlStr = '';
  
  function artistTemplate(artist){
    var img;
    if(artist.images.length > 0){
      img = artist.images[0].url
    }else {
      img = 'https://alpha.aeon.co/assets/avatar-placeholder-1595e26c68347a14a51c9a5bcc34c6a26e97ff5218ce74c79db21df9fb808210.png';
    }
    htmlStr += '<div class="artist-container" data-uid="' + artist.id + '">' + 
                  '<div class="artist-pic-container">' + 
                    '<img class="artist-pic" src="' + img + '">' + 
                  '</div>' +
                  '<h4 class="artist-name">' + artist.name + '</h4>' +
                '</div>';
  }
  
  function albumTemplate(album){
    htmlStr += '<h4 class="album-title" data-uid="' + album.id + '">' + album.name + '</h4>' + 
              '<ol class="song-list"></ol>';
  }
  
  function trackListTemplate(song){
    htmlStr += '<li class="album-title"><a href="'+ song.preview_url + '" target="_blank">' + song.name + '</a></li>';

  }
  
  function showSuggestions(ev){
    var q = searchInput.value || '%20&type=artist';
    function render(val){
      htmlStr = '';
      val.artists.items.forEach(artistTemplate);
    results.innerHTML = htmlStr;    
    }
    AJAX.getData(APP.urls.main, q+'&type=artist', function(){APP.ajaxHandler(render)});
  }
  
  function showArtistsAlbums(ev){
    var target = ev.target;
    var artistID = target.parentElement.parentElement.dataset.uid;
    if(target.classList.contains('artist-pic')){
      function render(val){
        htmlStr = '';
        val.items.forEach(albumTemplate);
        albums.innerHTML = htmlStr;
      }
      AJAX.getData(APP.urls.artist, artistID+'/albums', function(){APP.ajaxHandler(render)});
      albums.parentElement.style.left = '0';
    }
  }

  function showSongs(ev){
    var target = ev.target;  
    var albumID = target.dataset.uid;
    if(target.classList.contains('album-title')){ 
      var songList = target.nextElementSibling;
      function render(val){
        htmlStr = '';
        val.items.forEach(trackListTemplate);
        songList.innerHTML = htmlStr;
      }
      if(songList.loaded){
        if(songList.visible){
          songList.style.display = 'none';
        }else {
          songList.style.display = '';
        }
        songList.visible = !songList.visible;
      }else {
        AJAX.getData(APP.urls.album, albumID+'/tracks', function(){APP.ajaxHandler(render)});
        songList.loaded = true;
        songList.visible = true;
      }
    }
  }
  
  function init(){
    searchInput.addEventListener('keyup', showSuggestions, false);
    results.addEventListener('click', showArtistsAlbums, false);
    albums.addEventListener('click', showSongs, false);
    albums.previousElementSibling.addEventListener('click', function(ev){
      this.parentElement.style.left = '-300px';
    }, false);
  }
  
  return {
    init: init
  }
  
})();